<?php

namespace App\Http\Controllers;

use App\Models\Book;
use App\Models\Borrow;
use App\Models\Student;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Ramsey\Uuid\Uuid;

class BorrowController extends Controller
{
    public function __construct()
    {
        $this->middleware("auth:api");
    }

    public function list()
    {
        $borrows = Borrow::with("user", "student", "book")->get();
        return response()->json($borrows);
    }

    public function detail($borrow_id)
    {
        $borrow = Borrow::with("user", "student", "book")->where("borrow_id", $borrow_id)->first();
        if (!$borrow) {
            abort(404);
        }

        return response()->json($borrow);
    }

    public function return($borrow_id)
    {
        $borrow = Borrow::with("user", "student", "book")->where("borrow_id", $borrow_id)->where("is_returned", 0)->firstOrFail();
        if (!$borrow) {
            abort(404);
        }

        $borrow->is_returned = 1;
        $borrow->save();

        return response()->json($borrow);
    }

    public function save(Request $r)
    {
        $r->validate([
            "student_id" => "required|string",
            "book_id" => "required|string",
            "returned_at" => "required|date|after:now",
        ]);

        $borrow = DB::transaction(function () use ($r) {
            $book = Book::where("is_active", 1)->where("stock", ">", 0)->findOrFail($r->book_id);
            $book->stock -= 1;
            $book->save();

            Student::findOrFail($r->student_id);

            $borrow = Borrow::create([
                "borrow_id" => Uuid::uuid1()->toString(),
                "reason" => $r->reason,
                "user_id" => Auth::user()->id,
                "student_id" => $r->student_id,
                "book_id" => $r->book_id,
                "borrowed_at" => date("Y-m-d H:i:s"),
                "returned_at" => $r->returned_at,
            ]);
            return $borrow;
        });

        return response()->json($borrow, 201);
    }
}
