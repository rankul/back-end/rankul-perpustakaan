<?php

namespace App\Http\Controllers;

use App\Models\Book;
use Illuminate\Http\Request;

class BookController extends Controller
{
    public function __construct()
    {
        $this->middleware("auth:api");
    }

    public function list(Request $r)
    {
        //TODO: Implemen List Buku dengan search by title, description, is available.
    }

    public function detail($id)
    {
        //TODO: Implemen get detail buku beserta data pinjam nya
    }

    public function save(Request $r)
    {
        $r->validate([
            "title" => "required|string",
            "description" => "required|string",
            "year_published" => "required|integer",
            "stock" => "required|integer",
        ]);

        $book = new Book;

        if ($r->id) {
            $book = Book::findOrFail($r->id);
        }

        $book->title = $r->title;
        $book->description = $r->description;
        $book->year_published = $r->year_published;
        $book->stock = $r->stock;
        $book->save();

        return response()->json($book, 201);
    }

    public function activate($id)
    {
        //TODO: Implemen Activate dan Deactivate buku dengan mengatur kolom `is_active` menjadi 0 atau 1. Seperti Toggle.
    }
}
