<?php

use App\Http\Controllers\BookController;
use App\Http\Controllers\BorrowController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get("book", BookController::class . "@list");
Route::get("book/{id}", BookController::class . "@detail");
Route::post("book", BookController::class . "@save");
Route::get("book/activate/{id}", BookController::class . "@activate");

Route::get("borrow", BorrowController::class . "@list");
Route::get("borrow/{borrow_id}", BorrowController::class . "@detail");
Route::post("borrow", BorrowController::class . "@save");
Route::get("borrow/return/{borrow_id}", BorrowController::class . "@return");
