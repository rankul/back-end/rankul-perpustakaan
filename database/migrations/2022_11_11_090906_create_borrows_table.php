<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('borrows', function (Blueprint $table) {
            $table->id();
            $table->string("borrow_id")->unique();
            $table->text("reason")->nullable();
            $table->unsignedBigInteger("user_id");
            $table->unsignedBigInteger("student_id");
            $table->unsignedBigInteger("book_id");
            $table->dateTime("borrowed_at");
            $table->dateTime("returned_at");
            $table->boolean("is_returned")->default(0);
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('student_id')->references('id')->on('students');
            $table->foreign('book_id')->references('id')->on('books');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('borrows');
    }
};
