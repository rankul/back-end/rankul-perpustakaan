<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Book>
 */
class BookFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            "title" => $this->faker->name,
            "description" => $this->faker->text(10),
            "year_published" => rand(1900, 2022),
            "stock" => rand(1000, 2000),
            "is_active" => 1,
        ];
    }
}
