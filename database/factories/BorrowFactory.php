<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Borrow>
 */
class BorrowFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            "borrow_id" => Str::random(5),
            "reason" => $this->faker->text(10),
            "user_id" => rand(1000, 2000),
            "student_id" => rand(1000, 2000),
            "book_id" => rand(1000, 2000),
            "borrowed_at" => date("Y-m-d H:i:s"),
            "returned_at" => date("Y-m-d H:i:s", strtotime("tomorrow")),
            "is_returned" => 0,
        ];
    }
}
