# Rankul Perpustakaan

Silakan clone dulu projek ini dengan command:

```
git clone git@gitlab.com:rankul/back-end/rankul-perpustakaan.git
```

Kemudian lakukan checkout ke branch baru pribadi dengan command:

```
git checkout -b test/<nama_singkat_kalian>
```

Branch? Apa itu?

Jadi projekan bisa beberapa versi. Namanya bebas seharusnya. Default branch biasanya `master` atau `main`. Jadi kalian mengedit projek yang sama kemudian mengumpulkannya dengan branch lain. Ada juga **Merge Request** atau **Pull Request**, dengan itu kalian bisa meminta agar branch yang kalian buat untuk digabungkan ke branch yang lain, bisa master, main atau branch tertentu. Untuk saat ini tidak perlu buat MR atau PR.

Branch diatas berarti `test/<nama_singkat_kalian>`

Kalau bingung lagi di branch apa tinggal command:

```
git status
```

Sebelum edit - edit projeknya. Lakukan command:

```
composer install
```

Untuk menginstall package pada projek laravel sesuai dengan file `composer.json`. Tanda sudah atau belum, ada folder `vendor` baigan luar.

Kemudian buat configurasinya nya dengan menyalin file `.env.example` menjadi `.env`. Edit file `.env` sesuai kebutuhan, biasanya pada pengaturan koneksi database.

Kemudian lakukan command:

```
php artisan migrate
```

dan

```
php artisan passport:install
```

Command diatas sudah pernah dicoba dipertemuan sebelumnya.

Kemudian coba kalian edit - edit projeknya dengan syarat:

-   Kalian tidak boleh mengedit API dan test yang sudah dibuat. Nanti bakalan terlihat kalau itu dirubah atau tidak.
-   Kalian tidak boleh menambahkan atau mengedit migration.
-   Kalian tidak boleh menambahkan atau mengedit factory.
-   Kalian tidak boleh menambahkan model.
-   Sudah disediakan clue untuk menambahkan code yaitu dengan kata `TODO` pada file file tertentu.
-   Jika diperlukan mengedit model silakan.

Tujuan akhirnya kalian harus bisa:

-   Membuat API sesuai dengan test yang sudah dibuat.
-   Membuat test sesuai dengan API yang sudah dibuat.
-   Usahakan coverage pada testing diatas 80%. Kalau 100% mantap.

Jika dirasa sudah selesai, silakan kumpulkan projekan yang sudah teredit yaitu dengan.

```
git add .
```

Command diatas untuk menambahkan file apa saja yang akan dikumpulkan di gitlab. Untuk tanda titik maksudnya seluruh file akan di kumpulkan. Jika file atau folder tertentu, tinggal ubah saja titik tersebut ke file atau folder tertentu.

```
git commit -m "<Pesan kalian>"
```

Command di atas untuk melakukan commit pada file yang sudah di-add sebelumnya.

```
git push origin <branch-mu>
```

Command di atas untuk meng-upload projek branch tertentu ke gitlabnya.

Kalau mau tahu apakah testnya pass atau tidak coba buka gitlabnya ke bagian kiri ada `Repository > Branches` ada branch punya kalian. Klik di kanan ada icon jika di hover akan puncul pipelines. Kemudian klik job `unit_test`. Disana ada proses command instalasi sampai testing. Disana terlihat apakah sudah tuntas atau belum. Jika dirasa ternyata masih merah silang, bisa diedit lagi projeknya kemudian add, commit push lagi.