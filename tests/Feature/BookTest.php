<?php

namespace Tests\Feature;

use App\Models\Book;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Mockery;
use Tests\PassportTestCase;
use Tests\TestCase;
use Illuminate\Support\Str;

class BookTest extends PassportTestCase
{
    use DatabaseTransactions;
    /**
     * A basic test example.
     *
     * @return void
     */

    public function testListBookKeseluruhan()
    {
        $books = Book::factory()->count(10)->create();
        $response = $this->get("/api/book");
        $response->assertStatus(200)
            ->assertJsonStructure([
                [
                    "title",
                    "description",
                    "year_published",
                    "stock",
                    "updated_at",
                    "created_at",
                    "id",
                ]
            ])->assertJsonCount(10);
    }

    public function testListBookSesuaiTitleDanDescription()
    {
        $search = Str::random(4);
        Book::factory()->count(5)->create();
        Book::factory()->count(3)->create([
            "title" => Str::random(5) . $search . Str::random(5)
        ]);
        Book::factory()->count(3)->create([
            "description" => Str::random(5) . $search . Str::random(5)
        ]);

        $response = $this->get("/api/book?search=" . $search);
        $response->assertStatus(200)
            ->assertJsonStructure([
                [
                    "title",
                    "description",
                    "year_published",
                    "stock",
                    "updated_at",
                    "created_at",
                    "id",
                ]
            ])->assertJsonCount(6);
    }

    public function testListBookHanyaAvailable()
    {
        Book::factory()->count(3)->create([
            "stock" => 0,
            "is_active" => 0,
        ]);
        Book::factory()->count(3)->create([
            "stock" => 0,
            "is_active" => 1,
        ]);
        Book::factory()->count(3)->create([
            "stock" => rand(1, 10),
            "is_active" => 0,
        ]);
        Book::factory()->count(3)->create([
            "stock" => rand(1, 10),
            "is_active" => 1,
        ]);

        $response = $this->get("/api/book?available=true");
        $response->assertStatus(200)
            ->assertJsonStructure([
                [
                    "title",
                    "description",
                    "year_published",
                    "stock",
                    "updated_at",
                    "created_at",
                    "id",
                ]
            ])->assertJsonCount(3);
    }

    public function testListBookHanyaAktif()
    {
        Book::factory()->count(3)->create([
            "stock" => 0,
            "is_active" => 0,
        ]);
        Book::factory()->count(3)->create([
            "stock" => 0,
            "is_active" => 1,
        ]);
        Book::factory()->count(3)->create([
            "stock" => rand(1, 10),
            "is_active" => 0,
        ]);
        Book::factory()->count(3)->create([
            "stock" => rand(1, 10),
            "is_active" => 1,
        ]);

        $response = $this->get("/api/book?active=true");
        $response->assertStatus(200)
            ->assertJsonStructure([
                [
                    "title",
                    "description",
                    "year_published",
                    "stock",
                    "updated_at",
                    "created_at",
                    "id",
                ]
            ])->assertJsonCount(6);
    }

    public function testListBookHanyaTidakAktif()
    {
        Book::factory()->count(2)->create([
            "stock" => 0,
            "is_active" => 0,
        ]);
        Book::factory()->count(3)->create([
            "stock" => 0,
            "is_active" => 1,
        ]);
        Book::factory()->count(2)->create([
            "stock" => rand(1, 10),
            "is_active" => 0,
        ]);
        Book::factory()->count(3)->create([
            "stock" => rand(1, 10),
            "is_active" => 1,
        ]);

        $response = $this->get("/api/book?active=false");
        $response->assertStatus(200)
            ->assertJsonStructure([
                [
                    "title",
                    "description",
                    "year_published",
                    "stock",
                    "updated_at",
                    "created_at",
                    "id",
                ]
            ])->assertJsonCount(4);
    }

    public function testDetailBookBesertaBorrowsSukses()
    {
        $book = Book::factory()->create();

        $response = $this->get("/api/book/" . $book->id);
        $response->assertStatus(200)
            ->assertJson([
                "title" => $book->title,
                "description" => $book->description,
                "year_published" => $book->year_published,
                "stock" => $book->stock,
                "updated_at" => $book->updated_at,
                "created_at" => $book->created_at,
                "id" => $book->id,
            ])
            ->assertJsonStructure([
                "borrows" => [
                    [
                        "borrow_id",
                        "reason",
                        "user_id",
                        "student_id",
                        "book_id",
                        "borrowed_at",
                        "returned_at",
                        "is_returned",
                        "user" => [
                            "name",
                            "email",
                        ],
                        "student" => [
                            "name",
                            "class",
                        ],
                        "book" => [
                            "title",
                            "description",
                            "year_published",
                        ],
                    ]
                ]
            ]);
    }

    public function testDetailBookDataTidakAda()
    {
        $response = $this->get("/api/book/" . rand(1000, 2000));
        $response->assertStatus(404);
    }

    public function testActivateBookSuksesDariActive()
    {
        $book = Book::factory()->create([
            "is_active" => 1,
        ]);

        $response = $this->get("/api/book/activate/" . $book->id);
        $response->assertStatus(200)
            ->assertJson([
                "title" => $book->title,
                "description" => $book->description,
                "year_published" => $book->year_published,
                "stock" => $book->stock,
                "updated_at" => $book->updated_at,
                "created_at" => $book->created_at,
                "id" => $book->id,
                "is_active" => 0,
            ]);
        $this->assertDatabaseHas("books", [
            "id" => $book->id,
            "is_active" => 0,
        ]);
    }

    public function testActivateBookSuksesDariInactive()
    {
        $book = Book::factory()->create([
            "is_active" => 0,
        ]);

        $response = $this->get("/api/book/activate/" . $book->id);
        $response->assertStatus(200)
            ->assertJson([
                "title" => $book->title,
                "description" => $book->description,
                "year_published" => $book->year_published,
                "stock" => $book->stock,
                "updated_at" => $book->updated_at,
                "created_at" => $book->created_at,
                "id" => $book->id,
                "is_active" => 1,
            ]);
        $this->assertDatabaseHas("books", [
            "id" => $book->id,
            "is_active" => 1,
        ]);
    }

    public function testActivateBookGagalBukuTidakAda()
    {
        $book = Book::factory()->create([
            "is_active" => 0,
        ]);

        $response = $this->get("/api/book/activate/" . rand(1000, 2000));
        $response->assertStatus(404);

        $this->assertDatabaseHas("books", [
            "id" => $book->id,
            "is_active" => 0,
        ]);
    }

    //TODO: Buat test save buku
}
